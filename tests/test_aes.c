#include "../aes.h"

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
	byte i;
	size_t lena, lenb;
	byte msga[] = "Testing AES128 with OFB mode, this implementation is intended to be slightly faster than the aes_min one.\n";
	byte msgb[] = "Testing AES128 with CTR mode, this implementation is intended to be slightly faster than the aes_min one.\n";
	byte key[176] = { 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x12 };
	byte iv[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	byte ivc[16];

	lena = strlen(msga);
	lenb = strlen(msgb);

	aes_key_init(key);

	memcpy(ivc, iv, 16);
	aes_encrypt_ofb(msga, lena, key, ivc);

	memcpy(ivc, iv, 16);
	aes_encrypt_ctr(msgb, lenb, key, ivc);

	printf("echo 0 ");
	for (i = 0; i < lena; ++i)
		printf("%02X", msga[i]);
	printf(" | xxd -r -c 256 | openssl enc -aes-128-ofb -K \"12345612345612345612345612345612\" -iv \"00000000000000000000000000000000\"\n");

	printf("echo 0 ");
	for (i = 0; i < lenb; ++i)
		printf("%02X", msgb[i]);
	printf(" | xxd -r -c 256 | openssl enc -aes-128-ctr -K \"12345612345612345612345612345612\" -iv \"0000000000000000\"\n");

	return 0;
}
