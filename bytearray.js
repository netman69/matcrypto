/* ByteArray
 *
 * Simple class to help use arrays of bytes. It is a normal array with different constructor and extra functions.
 *
 * Constructor arguments can be a string or an array of bytes, strings and byte arrays (they'll be concatenated).
 * Empty constructor creates empty ByteArray.
 *
 * Functions:
 *   .toString([format]):
 *     If no argument provided gives the contents of the array as hex.
 *     Takes 1 argument, that sets the format to either of "hex", "base64", "base64url" or "string".
 *     "string" is meant for when the content of the array represents charcodes, the others should be self-explanatory.
 *
 * Static functions:
 *   .fromHex(str): Create a ByteArray from a hex string.
 *   .fromBase64(str): Create a ByteArray from a base64 or base64url string.
 *
 */
 window.ByteArray = (function(){
	function ByteArray() {
		var ret = [];
		for (var method in ByteArray.prototype)
			ret[method] = ByteArray.prototype[method];
		for (var x in arguments) {
			var arg = arguments[x];
			if (typeof arg === "string") {
				for (var i = 0; i < arg.length; ++i)
					ret.push(arg.charCodeAt(i));
			} else if (arg != undefined) {
				for (var i = 0; i < arg.length; ++i) {
					if (typeof arg[i] === "string") {
						for (var j = 0; j < arg[i].length; ++j)
							ret.push(arg[i].charCodeAt(j));
					} else if (typeof arg[i] === "array") {
						ret = ret.concat(arg[i]);
					} else {
						ret.push(arg[i])
					}
				}
			}
		}
		return ret;
	}

	ByteArray.prototype = {
		toString: function(format) {
			var base64 = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
							   "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
							   "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ];

			if (format == "base64") {
				return toBase64(base64.concat([ "+", "/" ]), this);
			} else if (format == "base64url") {
				return toBase64(base64.concat([ "-", "_" ]), this);
			} else if (format == "hex" || format == undefined) {
				var ret = "";
				for (var i = 0; i < this.length; ++i)
					ret += toHex(this[i]);
				return ret;
			} else if (format == "string") {
				var ret = "";
				for (var i = 0; i < this.length; ++i)
					ret += String.fromCharCode(this[i]);
				return ret;
			} throw "Invalid argument";

			function toBase64(table, str) {
				var ret = "";
				for (var i = 0; i < str.length; i += 3) {
					var a = str[i + 0], b = str[i + 1], c = str[i + 2];
					ret += table[(a >>> 2) & 0x3F] +
						   table[((a << 4) & 0x30) | ((b >>> 4) & 0x0F)] +
						   table[((b << 2) & 0x3C) | ((c >>> 6) & 0x03)] +
						   table[c & 0x3F];
				}
				return ret.substring(0, ret.length - [ 0, 2, 1 ][str.length % 3]) + [ "", "==", "=" ][str.length % 3];
			}

			function toHex(n) {
				var ret = n.toString(16);
				while (ret.length < 2)
					ret = "0" + ret;
				return ret;
			}
		},
	};

	ByteArray.fromHex = function(str) {
		var ret = new ByteArray();
		for (var i = 0; i < str.length; i += 2) {
			var s = str.substring(i, i + 2);
			while (s.length < 2)
				s += "0";
			ret.push(parseInt(s, 16));
		}
		return ret;
	}

	ByteArray.fromBase64 = function(str) {
		/* Implemented for portability, I know that browsers have functions for base64 builtin */
		var ret = new ByteArray();
		var base64dec = { "A":  0, "B":  1, "C":  2, "D":  3, "E":  4, "F":  5, "G":  6, "H":  7, "I":  8, "J":  9,
		                  "K": 10, "L": 11, "M": 12, "N": 13, "O": 14, "P": 15, "Q": 16, "R": 17, "S": 18, "T": 19,
		                  "U": 20, "V": 21, "W": 22, "X": 23, "Y": 24, "Z": 25, "a": 26, "b": 27, "c": 28, "d": 29,
		                  "e": 30, "f": 31, "g": 32, "h": 33, "i": 34, "j": 35, "k": 36, "l": 37, "m": 38, "n": 39,
		                  "o": 40, "p": 41, "q": 42, "r": 43, "s": 44, "t": 45, "u": 46, "v": 47, "w": 48, "x": 49,
		                  "y": 50, "z": 51, "0": 52, "1": 53, "2": 54, "3": 55, "4": 56, "5": 57, "6": 58, "7": 59,
		                  "8": 60, "9": 61, "+": 62, "/": 63, "-": 62, "_": 63 };
		var s = 0, n = 0;
		for (var i = 0; i < str.length; ++i) {
			var c = base64dec[str.substring(i, i + 1)];
			if (c != undefined) {
				++s;
				n = n * 64 + c;
				if (s == 4) {
					ret.push((n >>> 16) & 0xFF);
					ret.push((n >>> 8) & 0xFF);
					ret.push(n & 0xFF);
					s = 0;
					n = 0;
				}
			}
		}
		if (s == 2)
			ret.push(((n * 64 * 64) >>> 16) & 0xFF);
		if (s == 3) {
			n *= 64;
			ret.push((n >>> 16) & 0xFF);
			ret.push((n >>> 8) & 0xFF);
		}
		return ret;
	}

	return ByteArray;
}).call({});
