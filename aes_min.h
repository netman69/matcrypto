/*
 * Minimalistic AES128 implementation, optimized for RAM usage. Supports OFB and CTR mode.
 *
 * Based on the work of Chris Hulbert (chris.hulbert@gmail.com, http://splinter.com.au/blog)
 * Modified into this by Mattis Michel.
 * Taken from https://github.com/chrishulbert/crypto/
 * This code is public domain, or any OSI-approved license, your choice. No warranty.
 *
 */

#ifndef __AES_MIN_H__
#define __AES_MIN_H__

#include <stdint.h> /* uint8_t, uint32_t */
#include <stddef.h> /* size_t */

typedef uint8_t byte;

/* Encrypt a single 128 bit block by a 128 bit key using AES */
extern void aes_encrypt(byte *msg, byte *key);

/* Encrypt data in OFB mode, key and iv must be 16 bytes */
extern void aes_encrypt_ofb(byte *data, size_t len, byte *key, byte *iv);

/* Encrypt data in CTR mode, key and iv must be 16 bytes, first 8 iv bytes are the nonce the next 8 are the initial counter */
/* This uses 16 more bytes of memory, but random-access is supported in this mode of operation, and errors do not propagate */
extern void aes_encrypt_ctr(byte *data, size_t len, byte *key, byte *iv);

/* Both aes_encrypt_* functions will update the iv, and put the output in data */
/* They can be chained as long as the lengths come in multiples of 16 */

#endif /* __AES_MIN_H__ */
